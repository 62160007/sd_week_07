/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_07;

import java.util.*;

/**
 *
 * @author tatar
 */
public class UserService {

    private static ArrayList<User> userList = null;

    static {
        userList = new ArrayList<>();
        // Mock Data
        userList.add(new User("root", "0000"));
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
        userList.add(new User("user2", "password"));
    }

    // Create user
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    // Delete user
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    // Read user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    public static User getUser(int index) {
        return userList.get(index);
    }

    // Update user
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    // File 
    public static void save() {

    }

    public static void load() {

    }

}
